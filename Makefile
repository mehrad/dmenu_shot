# To debug the issues, start with checking if you have mistakenly used spaces
# instead of tabs (It shows the presence of tabs with `^I` and line endings
# with `$`):
#  cat -e -t -v Makefile
# Source: https://stackoverflow.com/a/16945143/1613005


SHELL = /bin/sh

# define a variable to store the dependencies
REQUIRED_BINS := sh bash xclip flameshot dmenu convert xdotool

# define the path dmenu_shot should be installed
PREFIX ?= ${HOME}/.local/bin

# define a newline character to be used in messages
define LINEBREAK


endef


COLOR ?= TRUE

ifeq ($(COLOR),TRUE)
	COLOR_RESET :=$(shell tput sgr0)
	COLOR_ERROR :=$(shell tput setaf 1)
	COLOR_SUCCESS :=$(shell tput setaf 2)
	COLOR_MESSAGE :=$(shell tput setaf 3)
else
	COLOR_RESET :=
	COLOR_ERROR :=
	COLOR_SUCCESS :=
	COLOR_MESSAGE :=
endif


.PHONY: install

all help:
	$(info --------------------------------------------------------------------------------)
	$(info Available arguments:)
	$(info - "make install" to install)
	$(info - "make remove"  to uninstall (remove))
	$(info - "make check"   to check if you have all dependencies installed)
	$(info - "make help"    to show this help)
	$(info )
	$(info You can turn off colorizing the make output by $(COLOR_MESSAGE)"COLOR=FALSE"$(COLOR_RESET))
	$(info --------------------------------------------------------------------------------)
#	to suppress the "make: 'all' is up to date." message
	@:

check:
#	checking if the dependencies are me# checking if the dependencies are mett
	$(foreach bin,$(REQUIRED_BINS),\
		$(if $(shell command -v $(bin) 2> /dev/null),$(info $(COLOR_SUCCESS)[Ok]$(COLOR_RESET) Found `$(bin)`),$(error ${LINEBREAK}[Error] Missing Dependency. Please install `$(bin)`)))
	@if [ -f "${PREFIX}/dmenu_shot" ]; then \
		echo "${COLOR_MESSAGE}[NOTE]${COLOR_RESET} dmenu_shot is ${COLOR_SUCCESS}already${COLOR_RESET} installed"; \
	else \
		echo "${COLOR_MESSAGE}[NOTE]${COLOR_RESET} dmenu_shot is ${COLOR_ERROR}NOT${COLOR_RESET} installed yet"; \
	fi
	@:


install: check
	@cp ./dmenu_shot.sh ./dmenu_shot
	install --target "${PREFIX}" -D -m755 dmenu_shot
	@rm ./dmenu_shot
	@if [ -f "${PREFIX}/dmenu_shot" ] ; then \
		echo "${COLOR_SUCCESS}[Ok]${COLOR_RESET} Successfully installed. Now you can use dmenu_shot as a command :)"; \
	else \
		echo "${COLOR_ERROR}[Error]${COLOR_RESET} Pathetically failed to install"; \
	fi
	@:

remove uninstall:
	@if [ -f "${PREFIX}/dmenu_shot" ]; then \
		echo "${COLOR_SUCCESS}[Ok]${COLOR_RESET} Found the dmenu_shot. Going to remove ..."; \
		rm "${PREFIX}/dmenu_shot"; \
		if [ -f "${PREFIX}/dmenu_shot" ]; then \
			echo "${COLOR_ERROR}[Error]${COLOR_RESET} Pathetically failed to remove."; \
		else \
			echo "${COLOR_SUCCESS}[Ok]${COLOR_RESET} Successfully removed."; \
		fi \
	else \
		echo "${COLOR_ERROR}[Error]${COLOR_RESET} dmenu_shot was not found to be removed/uninstalled!"; \
	fi
	@:

